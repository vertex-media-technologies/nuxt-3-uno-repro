import presetUno from '@unocss/preset-uno'
import { theme } from '@unocss/preset-mini'
// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: false },
  modules: [
    '@unocss/nuxt',
    '@nuxtjs/critters'
  ],
  unocss: {
    // presets
    presets: [
      presetUno(),
    ],
    theme: {
      colors: {
        primary: {
          DEFAULT: '#FF6B27'
        },
        surface: {
          DEFAULT: '#FFFFFF',
          dark: '#1C1243'
        },
        success: {
          DEFAULT: theme.colors.emerald[500]
        }
      }
    },
    // core options
    shortcuts: [],
    rules: [],
  },
  css: [
    // Unocss browser preflight/reset
    '@unocss/reset/tailwind.css',
    // Main css file in the project
    '@/assets/css/main.css'
  ]
})
